package com.automation.framework.IntegrationTests;

import com.automation.framework.ControlImplementation.Container;
import com.automation.framework.Locators.Headers;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class IT_LoginTest {
    @Autowired
    Container container;

    @BeforeEach
    public void startUp() {
        log.info("Starting the test...");
        container.browserControl.start();
        container.browserControl.navigate("https://demoqa.com/login");
    }

    @Test
    void validLogin() {
        try {
            container.loginPage.enterUserName("sujay");
            container.loginPage.enterPassword("Qwerty!@#45");
            container.loginPage.clickOnLogin();
            Thread.sleep(2000);
            Assertions.assertEquals("Profile",container.headerControl.getHeaderText(Headers.PROFILE.getValue()));
        } catch (Exception e) {
            log.error("Exception occurred",e);
        }
    }

    @Test
    void invalidLogin() {
        try {
            container.loginPage.enterUserName("sujay");
            container.loginPage.enterPassword("Qwerty12345");
            container.loginPage.clickOnLogin();
            Assertions.assertEquals("Invalid username or password!", container.loginPage.getErrorMessage());
        } catch (Exception e) {
            log.error("Exception occurred",e);
        }
    }

    @AfterEach
    void tearDown() {
        container.browserControl.tearDown();
    }
}