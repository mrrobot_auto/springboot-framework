package com.automation.framework.APIControls.DTOs;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class BooksDto {
    private List<Book> books;
}
