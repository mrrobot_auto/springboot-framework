package com.automation.framework.APIControls;

import com.automation.framework.APIControls.DTOs.BooksDto;
import com.automation.framework.ControlImplementation.RequestBuilder;
import com.automation.framework.ControlImplementation.RestCall;
import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;

public class BooksAction {
    private URI uri = new URI("https://demoqa.com/BookStore/v1/Books");

    public BooksAction() throws URISyntaxException {
        // TODO document why this constructor is empty
    }

    public BooksDto getAllBooks() throws Exception {
        return new Gson().fromJson(RestCall.get(new RequestBuilder()
                .setUri(uri)
                .setToken(new TokenActions().generateToken().getToken())
                .build()), BooksDto.class);
    }
}
