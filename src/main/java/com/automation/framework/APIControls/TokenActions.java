package com.automation.framework.APIControls;


import com.automation.framework.APIControls.DTOs.AccessTokenDto;
import com.automation.framework.APIControls.DTOs.TokenRequestDto;
import com.automation.framework.ControlImplementation.RequestBuilder;
import com.automation.framework.ControlImplementation.RestCall;
import com.automation.framework.Exceptions.POMFWException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.springframework.stereotype.Component;

import java.net.URI;

@Component
public class TokenActions {
    private URI uri = URI.create("https://demoqa.com/Account/v1/GenerateToken");

    public URI getUri() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public AccessTokenDto generateToken() {
        try {
            TokenRequestDto tokenRequestDto = new TokenRequestDto();
            this.setUri(this.getUri());
            RequestBuilder req = new RequestBuilder()
                    .setUri(getUri())
                    .setEntity(new Gson().toJson(tokenRequestDto))
                    .build();
            return new Gson().fromJson(RestCall.post(req), AccessTokenDto.class);
        } catch (JsonSyntaxException | POMFWException e) {
            e.printStackTrace();
        }
        return null;
    }

}
