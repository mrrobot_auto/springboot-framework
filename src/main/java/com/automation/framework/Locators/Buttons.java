package com.automation.framework.Locators;

public enum Buttons {
    CLICK_ME(""),
    LOGIN("#login");
    private String value;
    Buttons(String value) {
        this.value = value;
    }

    public String getValue(){
        return value;
    }
}
