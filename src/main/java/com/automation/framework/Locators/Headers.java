package com.automation.framework.Locators;

public enum Headers {
    PROFILE("#app > div > div > div.pattern-backgound.playgound-header > div");
    private String value;
    Headers(String value) {
        this.value = value;
    }
    public String getValue(){
        return this.value;
    }
}
