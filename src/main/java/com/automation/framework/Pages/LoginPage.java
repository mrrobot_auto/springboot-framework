package com.automation.framework.Pages;


import com.automation.framework.Locators.Buttons;
import com.automation.framework.Locators.Messages;
import com.automation.framework.Locators.TextBox;
import com.automation.framework.UIControls.ButtonControl;
import com.automation.framework.UIControls.MessageControl;
import com.automation.framework.UIControls.TextBoxControl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginPage {
    @Autowired
    ButtonControl buttonControl;
    @Autowired
    TextBoxControl textBoxControl;
    @Autowired
    MessageControl messageControl;

    public void clickOnLogin() {
        buttonControl.click(Buttons.LOGIN.getValue());
    }

    public void enterUserName(String username) {
        textBoxControl.setText(TextBox.USER_NAME.getValue(), username);
    }

    public void enterPassword(String password) {
        textBoxControl.setText(TextBox.PASSWORD.getValue(), password);
    }

    public String getErrorMessage() {
        return messageControl.getMessage(Messages.NAME.getValue());
    }
}
