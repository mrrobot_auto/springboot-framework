package com.automation.framework.Exceptions;

public class POMFWException extends Exception {
    public POMFWException() {
        super("Page object exception.");
    }
}
