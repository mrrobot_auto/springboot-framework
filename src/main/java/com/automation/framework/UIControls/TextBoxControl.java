package com.automation.framework.UIControls;

import com.automation.framework.ControlImplementation.CoreControl;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class TextBoxControl {

    private WebElement findControl(String locator) {
        try {
            return CoreControl.getDriver().findElement(By.cssSelector(locator));
        } catch (Exception staleElementReferenceException) {
            staleElementReferenceException.printStackTrace();
        }
        return null;
    }

    public void setText(String locator, String text) {
        try {
            WebElement textbox = findControl(locator);
            if (textbox != null) {
                log.info("Setting text \""+text+"\" to "+locator);
                textbox.clear();
                textbox.sendKeys(text);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getText(String locator) {
        String res = null;
        try {
            WebElement textbox = findControl(locator);
            if (textbox != null) {
                log.info("Getting text from "+ locator);
                res = textbox.getText();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
