package com.automation.framework.UIControls;

import com.automation.framework.ControlImplementation.CoreControl;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
@Slf4j
@Component
public class BrowserControl {

    public void navigate(String url) {
        try {
            WebDriver driver = CoreControl.getDriver();
            if (isUrl(url)) {
                driver.navigate().to(url);
                //new UrlChecker().waitUntilAvailable(30L,TimeUnit.SECONDS);
            } else {
                log.info("URL is not valid");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isUrl(String url) {
        return true;
    }

    public void start() {
        try {
            log.info("Starting test with chrome browser...");
            System.setProperty("webdriver.chrome.driver", "D:\\POMFW\\src\\main\\resources\\drivers\\chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--ignore-ssl-errors=yes");
            options.addArguments("--ignore-certificate-errors");
            WebDriver driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(10L, TimeUnit.MINUTES);
            driver.manage().timeouts().pageLoadTimeout(10L, TimeUnit.MINUTES);
            driver.manage().timeouts().setScriptTimeout(10L, TimeUnit.MINUTES);

            driver.manage().window().maximize();
            CoreControl.getInstance().setDriver(driver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tearDown() {
        WebDriver driver = CoreControl.getDriver();
        if (driver != null) {
            log.info("Shutting down the chrome.");
            driver.quit();
        }
    }
}
