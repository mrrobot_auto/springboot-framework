package com.automation.framework.UIControls;

import com.automation.framework.ControlImplementation.CoreControl;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class ButtonControl {

    private WebElement findControl(String locator) {
        try {
            return CoreControl.getDriver().findElement(By.cssSelector(locator));
        } catch (Exception staleElementReferenceException) {
            staleElementReferenceException.printStackTrace();
        }
        return null;
    }

    public void click(String buttonNameLocator) {
        try {
            log.info("Clicking on "+buttonNameLocator);
            WebElement but = findControl(buttonNameLocator);
            if (but != null && but.isDisplayed() && but.isEnabled()) {
                but.click();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
