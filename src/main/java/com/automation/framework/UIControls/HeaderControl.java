package com.automation.framework.UIControls;

import com.automation.framework.ControlImplementation.CoreControl;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HeaderControl {
    private WebElement findControl(String locator) {
        try {
            return CoreControl.getDriver().findElement(By.cssSelector(locator));
        } catch (Exception staleElementReferenceException) {
            staleElementReferenceException.printStackTrace();
        }
        return null;
    }

    public String getHeaderText(String locator) {
        String res = null;
        try {
            WebElement textbox = findControl(locator);
            if (textbox != null) {
                res = textbox.getText();
                log.info("Getting header text from " + locator + " " + res);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
