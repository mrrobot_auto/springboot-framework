package com.automation.framework.ControlImplementation;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class CoreControl {
    private WebDriver driver;
    private static CoreControl instance;

    private CoreControl() {
        this.setDriver(driver);
    }

    public static WebDriver getDriver() {
        if (instance == null) {
            synchronized (CoreControl.class) {
                if (instance == null) {
                    log.info("Setting a webdriver instance.");
                    instance = new CoreControl();
                }
            }
        }
        return instance.driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public static CoreControl getInstance() {
        if (instance == null) {
            synchronized (CoreControl.class) {
                if (instance == null) {
                    log.info("Setting a webdriver instance.");
                    instance = new CoreControl();
                }
            }
        }
        return instance;
    }

}
