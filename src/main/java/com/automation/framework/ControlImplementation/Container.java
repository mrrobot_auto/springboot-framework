package com.automation.framework.ControlImplementation;

import com.automation.framework.APIControls.TokenActions;
import com.automation.framework.Pages.LoginPage;
import com.automation.framework.UIControls.BrowserControl;
import com.automation.framework.UIControls.HeaderControl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Container {
    @Autowired
    public LoginPage loginPage;
    @Autowired
    public BrowserControl browserControl;
    @Autowired
    public TokenActions tokenActions;
    @Autowired
    public HeaderControl headerControl;
}
