package com.automation.framework.ControlImplementation;

import com.automation.framework.Exceptions.POMFWException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
@Component
public class RestCall {
    public static String post(RequestBuilder builder) throws POMFWException {
        try {
            CloseableHttpResponse response;
            HttpPost request = new HttpPost(builder.getUri());
            request.setHeader("Authorization", builder.getToken());
            request.setEntity(new StringEntity(builder.getEntity(), ContentType.APPLICATION_JSON));
            CloseableHttpClient httpClientBuilder = HttpClients.custom().build();
            response = httpClientBuilder.execute(request);
            httpClientBuilder.close();
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            throw new POMFWException();
        }
    }
    public static String get(RequestBuilder builder){
        try{
            CloseableHttpResponse response;
            HttpGet request = new HttpGet(builder.getUri());
            request.setHeader(new BasicHeader("Authorization",builder.getToken()));
            CloseableHttpClient httpClient = HttpClients.custom().build();
            response = httpClient.execute(request);
            return EntityUtils.toString(response.getEntity());
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
